import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Fenetre {

	public Fenetre(){
		
		JFrame fenetre = new JFrame("modelisation");
		fenetre.setPreferredSize(new Dimension(300,300));
		JPanel panel = new JPanel();
		JLabel label = new JLabel("salut");
		panel.add(label);
		
		
		fenetre.getContentPane().add(panel);
		panel.repaint();
		
		
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre.pack();
		fenetre.setVisible(true);
		
	}
	
    public static void main(String args[]){
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                        new Fenetre();
                }
        });
    }	
}